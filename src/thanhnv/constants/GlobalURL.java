package thanhnv.constants;

public class GlobalURL {
    public static final String XSL_CATEGORY = "/WEB-INF/outbla/category.xsl";
    public static final String OUTPUT_CATEGORY = "/WEB-INF/outbla/category.xml";
    public static final String XSL_OUTBLA = "/WEB-INF/outbla/outbla.xsl";
    public static final String OUTPUT_OUTBLA = "/WEB-INF/outbla/outbla.xml";
    public static final String XSL_SWSW = "/WEB-INF/swsw/swsw.xsl";
    public static final String OUTPUT_SWSW = "/WEB-INF/swsw/swsw.xml";
    public static final String XSL_SWE = "/WEB-INF/swe/swe.xsl";
    public static final String OUTPUT_SWE = "/WEB-INF/swe/swe.xml";
    public static final String XSL_CASUAL = "/WEB-INF/casual/casual.xsl";
    public static final String OUTPUT_CASUAL = "/WEB-INF/casual/casual.xml";
}
